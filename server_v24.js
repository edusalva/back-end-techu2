var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');

// variable marco de aplicacion web + puerto
var app = express();
var port = process.env.PORT || 3000;
app.use(bodyParser.json()); // para enviar datos en el body de la petición

var URLbase = "/apiperu/v1/";
var newID = 0;

//var baseMlabURL = "http://localhost:3000/apiperu/v1/";
// 05-11-2018

var baseMLabURL = "https://api.mlab.com/api/1/databases/apiperu-jmcc/collections/";
var apikeyMLab = "apiKey=7Lgmp3HkdHo7T6AW0hspJSn8ddeUONkv"; //7Lgmp3HkdHo7T6AW0hspJSn8ddeUONkv


//Login ************************************

app.post(URLbase + 'login', function(req, res) {
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"email":"' + email + '","password":"' + password + '"}'
  clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" + query + "&l=1&" + apikeyMLab)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
        if (body.length == 1) // Login ok
        {
          clienteMlab = requestJSON.createClient(baseMLabURL + "/user")
          var cambio = '{"$set":{"logged":"true"}}'
          clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apikeyMLab, JSON.parse(cambio), function(errP, resP, bodyP) {
          res.send({"login":"ok", "userID":body[0].userID, "first_name":body[0].first_name, "last_name":body[0].last_name})
          })
        }
        else {
        res.status(404).send('Usuario no encontrado')
        }
    }
  })
})

// Logout
app.post(URLbase + 'logout', function(req, res) {
  var id = req.body.userID
  var query = 'q={"userID":' + id + ', "logged":"true"}'
  clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" + query + "&l=1&" + apikeyMLab)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
        if (body.length == 1) // Estaba logado
        {
          clienteMlab = requestJSON.createClient(baseMLabURL + "/user")
          var cambio = '{"$set":{"logged":"false"}}'
          clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apikeyMLab, JSON.parse(cambio), function(errP, resP, bodyP) {
            res.send({"logout":"ok", "userID":body[0].userID})
          })
        }
        else {
          res.status(200).send('Usuario no logado previamente')
        }
    }
  })
})


// ************************************************************

// Obtener todos los usuarios GET
app.get(URLbase + 'usuarios',
  function(req, res) {
    let filtro = 'f={"_id":0}&';
    let clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + filtro + apikeyMLab,
      function(err, resM, body) {
        if (!err) {
          res.send(body)
        }
  });
});


// Obtener usuario con 'id'
app.get(URLbase + 'usuarios/:id',
  function(req, res) {
    let filtro = 'f={"_id":0}&';
    let queryStringID = 'q={"userID":' + req.params.id + '}&';
    let clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + queryStringID + filtro + apikeyMLab,
      function(err, resM, body) {
        if (!err) {
          res.send(body)
        }
  });
});


  // POST 'user' mLab
  app.post(URLbase + 'usuarios',
   function(req, res){
     var clienteMlab = requestJSON.createClient(baseMLabURL);
     console.log(req.body);
     clienteMlab.get('user?' + apikeyMLab,
       function(error, respuestaMLab, body){
         newID = body.length + 1;
         console.log("newID:" + newID);
         var newUser = {
           "userID" : newID,
           "first_name" : req.body.first_name,
           "last_name" : req.body.last_name,
           "email" : req.body.email,
           "password" : req.body.password
         };
         clienteMlab.post(baseMLabURL + "user?" + apikeyMLab, newUser,
           function(error, respuestaMLab, body){
             console.log(body);
             res.send(body);
           });
       });
   });

   //PUT of user
   app.put(URLbase + 'usuarios/:id',
   function(req, res) {
     var id = req.params.id;
     var queryStringID = 'q={"userID":' + id + '}&';
     var clienteMlab = requestJSON.createClient(baseMLabURL);
     clienteMlab.get('user?'+ queryStringID + apikeyMLab ,
       function(error, respuestaMLab , body) {
        var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
        clienteMlab.put(baseMLabURL + 'user?q={"userID": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
         function(error, respuestaMLab, body) {
           console.log("body:"+ body);
          res.send(body);
         });
    });
   });

   //DELETE user with id
   app.delete(URLbase + "usuarios/:id",
     function(req, res){
       console.log("entra al DELETE");
       console.log("request.params.id: " + req.params.id);
       var id = req.params.id;
       var queryStringID = 'q={"userID":' + id + '}&';
       console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
       var httpClient = requestJSON.createClient(baseMLabURL);
       httpClient.get('user?' +  queryStringID + apikeyMLab,
         function(error, respuestaMLab, body){
           var respuesta = body[0];
           console.log("body delete:"+ respuesta);
           httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
             function(error, respuestaMLab,body){
               res.send(body);
           });
         });
     });


// Obtener todos los usuarios GET
app.get(URLbase + 'usuarios',
  function(req, res) {
    let clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?' + apikeyMLab,
      function(err, resM, body) {
        if (!err) {
          res.send(body)
        }
  });
});
app.listen(port);
